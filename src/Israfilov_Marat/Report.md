# Лабораторная работа №5. Полиномы

## Цели и задачи 


В рамках лабораторной работы ставится задача создания программных средств, поддерживающих эффективное представление полиномов и выполнение следующих операций над ними:

*	организация хранения полинома
*	удаление введенного ранее полинома;
*	копирование полинома;
*	сложение полиномов;
*   сравнение полиномов;
*   вывод;

Предполагается, что в качестве структуры хранения будут использоваться списки. В качестве дополнительной цели в лабораторной работе ставится также задача разработки некоторого общего представления списков и операций по их обработке. В числе операций над списками должны быть реализованы следующие действия:

*	поддержка понятия текущего звена;
*	вставка звеньев в начало, после текущей позиции и в конец списков;
*	удаление звеньев в начале и в текущей позиции списков;

## Структура проекта


![Структура проекта](https://bytebucket.org/Marat_Israfilov/mp2-lab5-poly/raw/3c9ef5643dfacd08e308c0076187ccda142280ae/src/Israfilov_Marat/images/struct.png)

С учетом всех перечисленных ранее требований может быть предложен следующий состав классов и отношения между этими классами (см. рис. 5.1):

*	класс TDatValue для определения класса объектов-значений списка (абстрактный класс);
*	класс TMonom для определения объектов-значений параметров монома;
*	класс TRootLink для определения звеньев (элементов) списка (абстрактный класс);
*	класс TDatLink для определения звеньев (элементов) списка с указателем на объект-значение;
*	класс TDatList для определения линейных списков;
*	класс THeadRing для определения циклических списков с заголовком;
*	класс TPolinom для определения полиномов.

## Классы

### Абстрактный класс объектов-значений TDatValue

```c++
class TDatValue;
typedef TDatValue* PTDatValue;

class TDatValue 
{
public:

	virtual PTDatValue GetCopy() = 0; // создание копии
	~TDatValue() {}

};
```

### Класса моном TMonom

```c++
#include "TDatValue.h"
#include <iostream>
using namespace std;

class TMonom;
typedef TMonom* PTMonom;

class TMonom : public TDatValue 
{
protected:

	int Coeff; // коэффициент монома
	int Index; // индекс (свертка степеней)

public:

	TMonom(int cval = 1, int ival = 0) 
	{
		Coeff = cval; 
		Index = ival;
	};

	virtual PTDatValue GetCopy() // изготовить копию
	{ 
		PTDatValue tmp = nullptr;
		tmp = new TMonom(Coeff, Index);
		if (tmp == nullptr)
			throw "Monom copy wasn't created";
		else		
			return tmp;
	};

	void SetCoeff(int cval) { Coeff = cval; }
	int  GetCoeff(void) { return Coeff; }
	void SetIndex(int ival) { Index = ival; }
	int  GetIndex(void) { return Index; }

	TMonom& operator=(const TMonom &tm) 
	{
		Coeff = tm.Coeff; 
		Index = tm.Index;
		return *this;
	}

	int operator==(const TMonom &tm) 
	{
		return (Coeff == tm.Coeff) && (Index == tm.Index);
	}

	int operator<(const TMonom &tm) 
	{
		return Index < tm.Index;
	}

	friend ostream& operator<<(ostream &os, TMonom &m)
	{
		os << m.GetCoeff() << "*" << "x^" << m.GetIndex() / 100 << "y^" << (m.GetIndex() % 100) / 10 << "z^" << m.GetIndex() % 10;
		return os;
	}

	friend class TPolynom;

};
```

### Базовый класс для звеньев (элементов) списка TRootLink

```c++
#include "TDatValue.h"

class TRootLink;
typedef TRootLink *PTRootLink;

class TRootLink 
{
protected:

	PTRootLink  pNext;  // указатель на следующее звено

public:

	TRootLink(PTRootLink pN = nullptr) : pNext(pN) {}

	PTRootLink  GetNextLink() { return  pNext; }
	void SetNextLink(PTRootLink  pLink) { pNext = pLink; }
	void InsNextLink(PTRootLink  pLink) 
	{
		PTRootLink p = pNext;  
		pNext = pLink;
		if (pLink != nullptr) 
			pLink->pNext = p;
	}

	virtual void       SetDatValue(PTDatValue pVal) = 0;
	virtual PTDatValue GetDatValue() = 0;

	friend class TDatList;

};
```

### Класс для звеньев (элементов) списка с указателем на объект-значение TDatLink

```c++
#include "TRootLink.h"

class TDatLink;
typedef TDatLink *PTDatLink;

class TDatLink : public TRootLink 
{
protected:

	PTDatValue pValue;  // указатель на объект значения

public:

	TDatLink(PTDatValue pVal = nullptr, PTRootLink pN = nullptr) : TRootLink(pN), pValue(pVal) {}
	
	void       SetDatValue(PTDatValue pVal) { pValue = pVal; }
	PTDatValue GetDatValue() { return  pValue; }
	PTDatLink  GetNextDatLink() { return  (PTDatLink)pNext; }

	friend class TDatList;

};
```

### Класс линейных списков TDatList

```c++
#include "TDatLink.h"

enum TLinkPos {FIRST, CURRENT, LAST};

class TDatList
{
protected:

	PTDatLink pFirst;    // первое звено
	PTDatLink pLast;     // последнее звено
	PTDatLink pCurrLink; // текущее звено
	PTDatLink pPrevLink; // звено перед текущим
	PTDatLink pStop;     // значение указателя, означающего конец списка 
	int CurrPos;         // номер текущего звена (нумерация от 0)
	int ListLen;         // количество звеньев в списке

protected:  // методы

	PTDatLink GetLink(PTDatValue pVal = nullptr, PTDatLink pLink = nullptr);
	void      DelLink(PTDatLink pLink);   // удаление звена

public:

	TDatList();
	~TDatList() { DelList(); }
	// доступ
	PTDatValue GetDatValue(TLinkPos mode = CURRENT) const; // значение
	virtual int IsEmpty()  const { return pFirst == pStop; } // список пуст ?
	int GetListLength()    const { return ListLen; }       // к-во звеньев
														   // навигация
	int SetCurrentPos(int pos);          // установить текущее звено
	int GetCurrentPos(void) const;       // получить номер тек. звена
	virtual int Reset(void);             // установить на начало списка
	virtual int IsListEnded(void) const; // список завершен ?
	int GoNext(void);                    // сдвиг вправо текущего звена
										 // (=1 после применения GoNext для последнего звена списка)
										 // вставка звеньев
	virtual void InsFirst(PTDatValue pVal = nullptr); // перед первым
	virtual void InsLast(PTDatValue pVal = nullptr); // вставить последним 
	virtual void InsCurrent(PTDatValue pVal = nullptr); // перед текущим 
													 // удаление звеньев
	virtual void DelFirst(void);    // удалить первое звено 
	virtual void DelCurrent(void);    // удалить текущее звено 
	virtual void DelList(void);    // удалить весь список

};
```

__Реализация__

```c++
#include "TDatList.h"
#include <iostream>

TDatList::TDatList() : ListLen(0)
{	
	pFirst = pLast = pStop = nullptr;
	Reset();
}

PTDatLink TDatList::GetLink(PTDatValue pVal, PTDatLink pLink)
{	
	PTDatLink tmp = nullptr;
	tmp = new TDatLink(pVal, pLink);
	if (tmp == nullptr)
		throw "Link can not be gotten";
	else
		return tmp;	
}

void TDatList::DelLink(PTDatLink pLink) 
{ 
	if (pLink != nullptr)
	{
		if (pLink->pValue != nullptr)
			delete pLink->pValue;
		delete pLink;
	}
}

PTDatValue TDatList::GetDatValue(TLinkPos mode) const
{
	PTDatLink tmp = nullptr;
	if (mode == CURRENT)
		tmp = pCurrLink;
	else if (mode == FIRST)
		tmp = pFirst;
	else if (mode == LAST)
		tmp = pLast;
	else	
		throw "Invalid type of position";

	if (tmp == nullptr)
		throw "DatValue can not be gotten";
	else
		return tmp->TDatLink::GetDatValue();
}

int TDatList::SetCurrentPos(int pos)
{
	if (pos >= ListLen)
		throw "Invalid value of position";
  	Reset();
	for (int i = 0; i < pos; i++, GoNext())
	return 0;
}

int TDatList::GetCurrentPos(void) const
{	
	return CurrPos;
}

int TDatList::Reset(void)
{
	pPrevLink = pStop;

	if (!IsEmpty())
	{		
		pCurrLink = pFirst;
		CurrPos = 0;		
	}
	else
	{
		pCurrLink = pStop;
		CurrPos = -1;
	}

	return 0;
}

int TDatList::IsListEnded(void) const
{	
	return pCurrLink == pStop;
}

int TDatList::GoNext(void)
{	
	int result;

	if (pCurrLink == pStop)
		result = 0;
	else
	{
		pPrevLink = pCurrLink;
		pCurrLink = pCurrLink->GetNextDatLink();
		CurrPos++;

		result = 1;
	}

	return result;
}

void TDatList::InsFirst(PTDatValue pVal)
{
	PTDatLink first = GetLink(pVal, pFirst);
	ListLen++;
	pFirst = first;

	if (ListLen == 1)
	{
		pLast = first;
		Reset();
	}
	else if (CurrPos == 0)
		pCurrLink = first;
	else
		CurrPos++;
}

void TDatList::InsLast(PTDatValue pVal)
{
	PTDatLink last = GetLink(pVal, pStop);
	
	if (pLast != nullptr) 
		pLast->SetNextLink(last);

	ListLen++;
	pLast = last;
	
	if (ListLen == 1)
	{
		pFirst = last;
		Reset();
	}

	if (IsListEnded()) 
		pCurrLink = last;
}

void TDatList::InsCurrent(PTDatValue pVal)
{
	if (IsEmpty() || (pCurrLink == pFirst))
		InsFirst(pVal);
	else if (IsListEnded())
		InsLast(pVal);
	else
	{
		PTDatLink current = GetLink(pVal, pCurrLink);
		pCurrLink = current;
		pPrevLink->SetNextLink(current);
		ListLen++;
	}
}

void TDatList::DelFirst(void)
{
	if (IsEmpty())
		throw "List is empty";

	PTDatLink first = pFirst;
	pFirst = pFirst->GetNextDatLink();
	DelLink(first);
	ListLen--;

	if (IsEmpty())
	{
		pLast = pStop;
		Reset();
	}
	else if (CurrPos == 0)
		pCurrLink = pFirst;
	else if (CurrPos == 1)
		pPrevLink = pStop;

	if (CurrPos > 0)
		CurrPos--;

}

void TDatList::DelCurrent(void)
{
	if (pCurrLink == pStop)
		throw "List is empty";

	if (pCurrLink == pFirst)
		DelFirst();
	else
	{
		PTDatLink current = pCurrLink;
		pCurrLink = pCurrLink->GetNextDatLink();
		pPrevLink->SetNextLink(pCurrLink);
		DelLink(current);
		ListLen--;

		if (pCurrLink == pLast)
		{
			pLast = pPrevLink;
			pCurrLink = pStop;
		}
	}
}

void TDatList::DelList(void)
{
	while (!IsEmpty()) 
		DelFirst();
	
	pFirst = pLast = pPrevLink = pCurrLink = pStop;
	CurrPos = -1;	
}
```

### Класс циклических списков с заголовком THeadRing

```c++
#include "TDatList.h"

class THeadRing : public TDatList 
{
protected:

	PTDatLink pHead;     // заголовок, pFirst - звено за pHead

public:

	THeadRing();
	~THeadRing();
	// вставка звеньев
	virtual void InsFirst(PTDatValue pVal = nullptr); // после заголовка
												   // удаление звеньев
	virtual void DelFirst(void);                 // удалить первое звено

};
```

__Реализация__

```c++
#include "THeadRing.h"
#include <iostream>

THeadRing::THeadRing() : TDatList()
{
	InsLast();
	pHead = pFirst;
	ListLen = 0;
	pStop = pHead;
	Reset();
	pFirst->SetNextLink(pFirst);
}

THeadRing::~THeadRing()
{
	DelList();
	DelLink(pHead);
	pHead = nullptr;
}

void THeadRing::InsFirst(PTDatValue pVal)
{
	TDatList::InsFirst(pVal);
	pHead->SetNextLink(pFirst);
}
												  
void THeadRing::DelFirst(void)
{
	TDatList::DelFirst();
	pHead->SetNextLink(pFirst);
}
```

### Класс полином TPolinom

```c++
#include "THeadRing.h"
#include "TMonom.h"
#include <iostream>
using namespace std;

class TPolynom : public THeadRing 
{
public:

	TPolynom(int monoms[][2] = nullptr, int km = 0); // конструктор
												  // полинома из массива «коэффициент-индекс»
	TPolynom(TPolynom &q);      // конструктор копирования

	PTMonom  GetMonom() { return (PTMonom)GetDatValue(); }

	TPolynom operator+(TPolynom &q); // сложение полиномов
	TPolynom& operator=(TPolynom &q); // присваивание
	bool operator==(TPolynom &q);

	friend ostream& operator<<(ostream &os, TPolynom &poly)
	{			
		for (poly.Reset(); !poly.IsListEnded(); poly.GoNext())
		{					
			os << *poly.GetMonom() << " + ";
		}

		os << "\b\b\t ";
		return os;
	}

};
```

__Реализация__

```c++
#include "TPolynom.h"
#include <iostream>

TPolynom::TPolynom(int monoms[][2], int km)
{
	PTMonom monom = new TMonom(0, -1);
	pHead->SetDatValue(monom);	
	for (int i = 0; i < km; ++i)
	{
		monom = new TMonom(monoms[i][0], monoms[i][1]);
		InsLast(monom);
	}
}
												 
TPolynom::TPolynom(TPolynom &q)
{
	PTMonom monom = new TMonom(0, -1);
	pHead->SetDatValue(monom);
	int qPos = q.GetCurrentPos();

	for (q.Reset(); !q.IsListEnded(); q.GoNext())
	{
		PTMonom monom = q.GetMonom();
		InsLast(monom->GetCopy());		
	}

	q.SetCurrentPos(qPos);
}

TPolynom TPolynom::operator+(TPolynom &q)
{
	TPolynom result(*this);	
	PTMonom CurrMonom, AddMonom, tmp;
	result.Reset();
	q.Reset();
	while (true)
	{
		CurrMonom = result.GetMonom();
		AddMonom = q.GetMonom();
		if (CurrMonom->Index < AddMonom->Index)
		{
			tmp = new TMonom(AddMonom->Coeff, AddMonom->Index);
			result.InsCurrent(tmp);
			q.GoNext();
		}
		else if (CurrMonom->Index > AddMonom->Index)
			result.GoNext();
		else
		{
			if (CurrMonom->Index == -1)
				break;
			CurrMonom->Coeff += AddMonom->Coeff;
			if (CurrMonom->Coeff != 0)
			{
				result.GoNext();
				q.GoNext();
			}
			else
			{
				result.DelCurrent();
				q.GoNext();
			}
		}
	}


	return result;
}

TPolynom& TPolynom::operator=(TPolynom &q)
{
	if (&q != this)
	{	
		PTMonom tmp = new TMonom(0, -1);
		DelList();
		q.Reset();
		pHead->SetDatValue(tmp);

		for (q.Reset(); !q.IsListEnded(); q.GoNext())
		{
			tmp = q.GetMonom();
			InsLast(tmp->GetCopy());			
		}
	}

	return *this;
}

bool TPolynom::operator==(TPolynom &q)
{
	if (GetListLength() != q.GetListLength())
		return false;
	else
	{
		TPolynom poly1, poly2;
		PTMonom pMon, qMon;
		Reset(); q.Reset();
		while (!IsListEnded())
		{
			pMon = GetMonom();
			qMon = q.GetMonom();
			if (*pMon == *qMon)
			{
				GoNext(); q.GoNext();
			}
			else
				return false;
		}
		return true;
	}
}
```

## Тестирование

```c++
#include "gtest.h"
#include "TPolynom.h"

TEST(TPolynom, can_create_polynom)
{
	int monom[][2] = { { 1, 0 } };	
	
	EXPECT_NO_THROW(TPolynom Res(monom, 1););
}

TEST(TPolynom, copied_polynom_is_equal_source)
{
	int monom[][2] = { { 1, 0 } };
	TPolynom P1(monom, 1);
	TPolynom P2(P1);

	EXPECT_TRUE(P1 == P2);
}

TEST(TPolynom, copied_polynom_has_its_own_memory)
{
	int monom[][2] = { { 1, 0 } };
	TPolynom P1(monom, 1);
	TPolynom P2(P1);

	EXPECT_NE(&P1, &P2);
}

TEST(TPolynom, assign_polynom_is_equal_source)
{
	int monom[][2] = { { 1, 0 } };
	TPolynom P1(monom, 1);
	TPolynom P2;

	P2 = P1;

	EXPECT_TRUE(P1 == P2);
}

TEST(TPolynom, assign_polynom_has_its_own_memory)
{
	int monom[][2] = { { 1, 0 } };
	TPolynom P1(monom, 1);
	TPolynom P2;

	P2 = P1;

	EXPECT_NE(&P1, &P2);
}

TEST(TPolynom, compare_is_correct)
{
	int monom1[][2] = { {5, 321}, {7, 213}, { 1, 0 } };
	int monom2[][2] = { { 5, 321 },{ 7, 213 } };
	TPolynom P1(monom1, 3);
	TPolynom P2(monom2, 2);
	
	EXPECT_FALSE(P1 == P2);
}

TEST(TPolynom, can_multiple_assign_polynoms)
{
	int monom[][2] = { { 1, 0 } };
	TPolynom P1(monom, 1);
	TPolynom P2;
	TPolynom P3;

	P3 = P2 = P1;

	EXPECT_TRUE(P1 == P3);
}

TEST(TPolynom, can_add_polynoms)
{
	int monom1[][2] = { { 5, 321 },{ 7, 223 },{ 1, 0 } };
	int monom2[][2] = { { 5, 321 }, { 17, 213 }, {2, 153} };
	int monom3[][2] = { { 10, 321 }, { 7, 223 }, { 17, 213 },{ 2, 153 },{ 1, 0 } };
	TPolynom P1(monom1, 3);
	TPolynom P2(monom2, 3);
	TPolynom res;

	res = P1 + P2;

	TPolynom expect(monom3, 5);

	EXPECT_TRUE(res == expect);
}

TEST(TPolynom, can_multiple_add_polynoms)
{
	int monom1[][2] = { { 5, 321 },{ 7, 223 },{ 1, 0 } };
	int monom2[][2] = { { 5, 321 },{ 17, 213 },{ 2, 153 } };
	int monom3[][2] = { {-10, 321}, {4, 313}, {5, 0} };
	int monom4[][2] = { {4, 313}, {7, 223}, {17, 213}, {2, 153}, {6, 0} };
 	TPolynom P1(monom1, 3);
	TPolynom P2(monom2, 3);
	TPolynom P3(monom3, 3);
	TPolynom res;

	res = P1 + P2 + P3;

	TPolynom expect(monom4, 5);

	EXPECT_TRUE(res == expect);
}
```

![Тесты](https://bytebucket.org/Marat_Israfilov/mp2-lab5-poly/raw/3c9ef5643dfacd08e308c0076187ccda142280ae/src/Israfilov_Marat/images/tests.png)

## Демострация программы

```c++
#include "TPolynom.h"
#include <iostream>
using namespace std;

int main()
{	
	int monom1[][2] = { { 5, 321 },{ 7, 223 } };
	int monom2[][2] = { { 5, 321 },{ 17, 213 },{ 2, 153 } };	
	TPolynom P1(monom1, 2);
	TPolynom P2(monom2, 3);
	TPolynom res;

	res = P1 + P2;
	
	cout << "First polinom:\t" << P1 << endl << endl;
	cout << "Second polinom:\t" << P2 << endl << endl;
	cout << "Result:\t" << res << endl;
	
	return 0;
}
```

![Демонстрация](https://bytebucket.org/Marat_Israfilov/mp2-lab5-poly/raw/3c9ef5643dfacd08e308c0076187ccda142280ae/src/Israfilov_Marat/images/demo.png)

## Вывод

В ходе выполнения данной работы была создана целая иерархия классов, реализующих динамическую структуру данных: __Списки__.

На её основе был создан класс __Полином__ в качестве звеньев которого выступает класс __Моном__.

В дальнейшем на основе этой структуры можно реализовать и другие классы.

Благодаря цикличности списков и наличию заговочного звена имеется возможность уйти от проверки нулевого указателя последнего звена списка.

![Цикличность](https://bytebucket.org/Marat_Israfilov/mp2-lab5-poly/raw/3c9ef5643dfacd08e308c0076187ccda142280ae/src/Israfilov_Marat/images/pic1.png)
