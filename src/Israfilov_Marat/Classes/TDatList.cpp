#include "TDatList.h"
#include <iostream>

TDatList::TDatList() : ListLen(0)
{	
	pFirst = pLast = pStop = nullptr;
	Reset();
}

PTDatLink TDatList::GetLink(PTDatValue pVal, PTDatLink pLink)
{	
	PTDatLink tmp = nullptr;
	tmp = new TDatLink(pVal, pLink);
	if (tmp == nullptr)
		throw "Link can not be gotten";
	else
		return tmp;	
}

void TDatList::DelLink(PTDatLink pLink) 
{ 
	if (pLink != nullptr)
	{
		if (pLink->pValue != nullptr)
			delete pLink->pValue;
		delete pLink;
	}
}

PTDatValue TDatList::GetDatValue(TLinkPos mode) const
{
	PTDatLink tmp = nullptr;
	if (mode == CURRENT)
		tmp = pCurrLink;
	else if (mode == FIRST)
		tmp = pFirst;
	else if (mode == LAST)
		tmp = pLast;
	else	
		throw "Invalid type of position";

	if (tmp == nullptr)
		throw "DatValue can not be gotten";
	else
		return tmp->TDatLink::GetDatValue();
}

int TDatList::SetCurrentPos(int pos)
{
	if (pos >= ListLen)
		throw "Invalid value of position";
	Reset();
	for (int i = 0; i < pos; i++, GoNext())
	return 0;
}

int TDatList::GetCurrentPos(void) const
{	
	return CurrPos;
}

int TDatList::Reset(void)
{
	pPrevLink = pStop;

	if (!IsEmpty())
	{		
		pCurrLink = pFirst;
		CurrPos = 0;		
	}
	else
	{
		pCurrLink = pStop;
		CurrPos = -1;
	}

	return 0;
}

int TDatList::IsListEnded(void) const
{	
	return pCurrLink == pStop;
}

int TDatList::GoNext(void)
{	
	int result;

	if (pCurrLink == pStop)
		result = 0;
	else
	{
		pPrevLink = pCurrLink;
		pCurrLink = pCurrLink->GetNextDatLink();
		CurrPos++;

		result = 1;
	}

	return result;
}

void TDatList::InsFirst(PTDatValue pVal)
{
	PTDatLink first = GetLink(pVal, pFirst);
	ListLen++;
	pFirst = first;

	if (ListLen == 1)
	{
		pLast = first;
		Reset();
	}
	else if (CurrPos == 0)
		pCurrLink = first;
	else
		CurrPos++;
}

void TDatList::InsLast(PTDatValue pVal)
{
	PTDatLink last = GetLink(pVal, pStop);
	
	if (pLast != nullptr) 
		pLast->SetNextLink(last);

	ListLen++;
	pLast = last;
	
	if (ListLen == 1)
	{
		pFirst = last;
		Reset();
	}

	if (IsListEnded()) 
		pCurrLink = last;
}

void TDatList::InsCurrent(PTDatValue pVal)
{
	if (IsEmpty() || (pCurrLink == pFirst))
		InsFirst(pVal);
	else if (IsListEnded())
		InsLast(pVal);
	else
	{
		PTDatLink current = GetLink(pVal, pCurrLink);
		pCurrLink = current;
		pPrevLink->SetNextLink(current);
		ListLen++;
	}
}

void TDatList::DelFirst(void)
{
	if (IsEmpty())
		throw "List is empty";

	PTDatLink first = pFirst;
	pFirst = pFirst->GetNextDatLink();
	DelLink(first);
	ListLen--;

	if (IsEmpty())
	{
		pLast = pStop;
		Reset();
	}
	else if (CurrPos == 0)
		pCurrLink = pFirst;
	else if (CurrPos == 1)
		pPrevLink = pStop;

	if (CurrPos > 0)
		CurrPos--;

}

void TDatList::DelCurrent(void)
{
	if (pCurrLink == pStop)
		throw "List is empty";

	if (pCurrLink == pFirst)
		DelFirst();
	else
	{
		PTDatLink current = pCurrLink;
		pCurrLink = pCurrLink->GetNextDatLink();
		pPrevLink->SetNextLink(pCurrLink);
		DelLink(current);
		ListLen--;

		if (pCurrLink == pLast)
		{
			pLast = pPrevLink;
			pCurrLink = pStop;
		}
	}
}

void TDatList::DelList(void)
{
	while (!IsEmpty()) 
		DelFirst();
	
	pFirst = pLast = pPrevLink = pCurrLink = pStop;
	CurrPos = -1;	
}