#include "gtest.h"
#include "TPolynom.h"

TEST(TPolynom, can_create_polynom)
{
	int monom[][2] = { { 1, 0 } };	
	
	EXPECT_NO_THROW(TPolynom Res(monom, 1););
}

TEST(TPolynom, copied_polynom_is_equal_source)
{
	int monom[][2] = { { 1, 0 } };
	TPolynom P1(monom, 1);
	TPolynom P2(P1);

	EXPECT_TRUE(P1 == P2);
}

TEST(TPolynom, copied_polynom_has_its_own_memory)
{
	int monom[][2] = { { 1, 0 } };
	TPolynom P1(monom, 1);
	TPolynom P2(P1);

	EXPECT_NE(&P1, &P2);
}

TEST(TPolynom, assign_polynom_is_equal_source)
{
	int monom[][2] = { { 1, 0 } };
	TPolynom P1(monom, 1);
	TPolynom P2;

	P2 = P1;

	EXPECT_TRUE(P1 == P2);
}

TEST(TPolynom, assign_polynom_has_its_own_memory)
{
	int monom[][2] = { { 1, 0 } };
	TPolynom P1(monom, 1);
	TPolynom P2;

	P2 = P1;

	EXPECT_NE(&P1, &P2);
}

TEST(TPolynom, compare_is_correct)
{
	int monom1[][2] = { {5, 321}, {7, 213}, { 1, 0 } };
	int monom2[][2] = { { 5, 321 },{ 7, 213 } };
	TPolynom P1(monom1, 3);
	TPolynom P2(monom2, 2);
	
	EXPECT_FALSE(P1 == P2);
}

TEST(TPolynom, can_multiple_assign_polynoms)
{
	int monom[][2] = { { 1, 0 } };
	TPolynom P1(monom, 1);
	TPolynom P2;
	TPolynom P3;

	P3 = P2 = P1;

	EXPECT_TRUE(P1 == P3);
}

TEST(TPolynom, can_add_polynoms)
{
	int monom1[][2] = { { 5, 321 },{ 7, 223 },{ 1, 0 } };
	int monom2[][2] = { { 5, 321 }, { 17, 213 }, {2, 153} };
	int monom3[][2] = { { 10, 321 }, { 7, 223 }, { 17, 213 },{ 2, 153 },{ 1, 0 } };
	TPolynom P1(monom1, 3);
	TPolynom P2(monom2, 3);
	TPolynom res;

	res = P1 + P2;

	TPolynom expect(monom3, 5);

	EXPECT_TRUE(res == expect);
}

TEST(TPolynom, can_multiple_add_polynoms)
{
	int monom1[][2] = { { 5, 321 },{ 7, 223 },{ 1, 0 } };
	int monom2[][2] = { { 5, 321 },{ 17, 213 },{ 2, 153 } };
	int monom3[][2] = { {-10, 321}, {4, 313}, {5, 0} };
	int monom4[][2] = { {4, 313}, {7, 223}, {17, 213}, {2, 153}, {6, 0} };
 	TPolynom P1(monom1, 3);
	TPolynom P2(monom2, 3);
	TPolynom P3(monom3, 3);
	TPolynom res;

	res = P1 + P2 + P3;

	TPolynom expect(monom4, 5);

	EXPECT_TRUE(res == expect);
}
